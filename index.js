
const FIRST_NAME = "Ioana-Mihaela";
const LAST_NAME = "Mitrache";
const GRUPA = "1092";

/**
 * Make the implementation here
 */

 class cache{
    //  constructor(){
    //  }

    pageAccessCounter(value)
    {
        if(value === undefined)
        {
            if(this.hasOwnProperty("home"))
                this["home"] += 1;
            else
                this["home"] = 1;
        }
        else
        {
            value = value.toLowerCase();
            if(this.hasOwnProperty(value))
                this[value] += 1;
            else 
                this[value] = 1;//creates a member and assigns the value 1 to it
        }
        
    }

    getCache()
    {
        return this;
    }

 }


function initCaching() {
    let test = new cache();
    return test;
}


module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

